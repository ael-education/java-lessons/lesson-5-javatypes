/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package ru.ael.lesson5types;

/**
 *
 * @author developer
 */
public class Lesson5Types {

    public static void main(String[] args) {
        System.out.println("Целочисленные типы данных");

        long b = 1555500000L; // 8 байт 
        System.out.println("Значение переменной b  = " + b); 
        
        int count = 5; // 4 байта обявление переменной и указание значения
        System.out.println("Значение переменной count  = " + count);  // преобразование cout  в строку  ctrl + пробел
        
        short a;  // 2 байта объявление переменной a 

        a = 2000; // запись в переменную a значения 2000 диапазон от -32768  до 32767
        System.out.println("Значение переменной a  = " + a);  

        a = 15;
        System.out.println("Значение переменной a  = " + a);

        
        byte  с  = 127; // 1 байт диапазон от -127  до 127
        System.out.println("Значение переменной с = " + с);
        
        
        System.out.println("Числа с плавающей точкой");
          
        float f = 3.14F;  // 4 байта 
        System.out.println("Значение переменной f  = " + f);
        
        double d = 124.786;  // 8 байтов
        System.out.println("Значение переменной f  = " + d);
        System.out.println("Резульаты математических операций: ");
        System.out.println("Положительная бесконечность:  "+Double.POSITIVE_INFINITY);
        System.out.println("Отричательная бесконечность:  "+Double.NEGATIVE_INFINITY);
        System.out.println("Не является числом:  "+Double.NaN); // NaN not a number
        
        
        System.out.println("Проверка Резульатов математических операций: ");
        if (Double.isNaN(d)) {
            System.out.println(" d Не является числом:  " + Double.NaN); // NaN not a number
        } else {
            System.out.println(" d является числом:  " + d); // NaN not a number
        }
        
        //
        System.out.println(" Присваивание значений с преобразованием типов cout = d:  ");
        
        System.out.println("Значение переменной d до присваивания  = " + d);
        System.out.println("Значение переменной count до присваивания  = " + count);
        count = (int) d;
        
        
        //  Код может не выполниться !
        //  Student s = new Student();
        //  Person p = (Person) s;
        // 
        //  p = s; //НЕЛЬЗЯ!!!
        
        
        System.out.println("Значение переменной count после присваивания  = " + count);
        
        System.out.println("Инкремент и декремен переменных ");
        int m = 0;
        int k = 9;
        System.out.println("Значение переменной k до присваивания  = " + k);        
        System.out.println("Значение переменной m до присваивания  = " + m);        
        m++;
        k = k + 1;
        System.out.println("Значение переменной m после присваивания = " + m);                
        System.out.println("Значение переменной k после присваивания = " + k);        
        k--;
        k--;
        k--;
        System.out.println("Значение переменной k после 3-х кратного декремента = " + k);        
        
        
    }
}
